<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Sheep;

class SheepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {

        $sheep = Sheep::all();
        if ($sheep->isEmpty()){
            $sum = 0;
            $paddock = [];
            while($sum !== 10){
                $paddock['one'] = rand(1, 10);
                $paddock['two'] = rand(1, 10);
                $paddock['three'] = rand(1, 10);
                $paddock['four'] = rand(1, 10);
                $sum = array_sum($paddock);
            }
            foreach ($paddock as $key => $pad ){
                $sheep = new Sheep;
                $sheep->paddock = $key;
                $sheep->numberOfShip = $pad;
                $sheep->save();
                $sheeps[] = $sheep;
            }
            return new JsonResponse($sheeps);
        }
        return new JsonResponse($sheep);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addSheep(Request $request)
    {
        $paddocksMin = Sheep::get()->where('numberOfShip', '=', 1);
        $res = [];
        if ($paddocksMin->isNotEmpty()){
            foreach ($paddocksMin as $key => $min){
                $paddocksMax = Sheep::orderBy('numberOfShip', 'DESC')->first();
                ++$min->numberOfShip;
                ++$min->moved;
                --$paddocksMax->numberOfShip;
                $paddocksMax->save();
                $min->save();
            }
        }
        $paddocks = Sheep::all();
        $random = $paddocks[rand(0,3)];
        $res['rand1'] = $random;
        ++$random->numberOfShip;
        ++$random->born;
        $random->save();



        return new JsonResponse($res);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return JsonResponse
     */
    public function kill()
    {
        $sheeps = Sheep::all();
        $random = rand(0,3);
       --$sheeps[$random]->numberOfShip;
       ++$sheeps[$random]->killed;
        $sheeps[$random]->save();
        return new JsonResponse($sheeps);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function destroy()
    {
        Sheep::query()->delete();
    }



}
